# Copyright 2021-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ user=scarpetta tag=v${PV} suffix=tar.bz2 new_download_scheme=true ] \
    cmake \
    freedesktop-desktop \
    gtk-icon-cache

SUMMARY="Simple and lightweight application to perform common editing operations on PDF files"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: graphicsmagick imagemagick ) [[
        number-selected = at-most-one
    ]]
    ( providers: qt5 qt6 ) [[ number-selected = exactly-one ]]
"

QT5_MIN_VER="5.11.0"

DEPENDENCIES="
    build:
        virtual/pkg-config
        providers:qt5? (
            x11-libs/qttools:5[>=${QT5_MIN_VER}] [[ note = [ lrelease ] ]]
        )
        providers:qt6? (
            x11-libs/qttools:6 [[ note = [ lrelease ] ]]
        )
    build+run:
        app-text/qpdf[>=10.0.0]
        providers:graphicsmagick? ( media-gfx/GraphicsMagick )
        providers:imagemagick? ( media-gfx/ImageMagick )
        providers:qt5? (
            x11-libs/qtbase:5[>=${QT5_MIN_VER}][gui]
            x11-libs/qtsvg:5[>=${QT5_MIN_VER}]
        )
        providers:qt6? (
            x11-libs/qtbase:6[gui]
            x11-libs/qtsvg:6
        )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/bd5f78c3a4d977d9b0c74302ce2521c737189b43.patch
    "${FILES}"/${PN}-1.1.1-fix-missing-definitions.patch
)

CMAKE_SOURCE=${WORKBASE}/${PN}-v${PV}

src_configure() {
    local cmakeparams=(
        -DQT_VERSION:STRING=$(option providers:qt6 6 5)
        -DUSE_GRAPHICSMAGICK:BOOL=$(option providers:graphicsmagick TRUE FALSE)
    )

    ecmake "${cmakeparams[@]}"
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

