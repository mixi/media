# Copyright 2017 Rasmus Thomsen
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'openh264-1.7.0.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

require github [ user=cisco tag=v${PV} ]

SUMMARY="Cisco OpenH264 library and Gecko Media Plugin for Mozilla packages"
HOMEPAGE="https://www.openh264.org/"
DOWNLOADS+=" https://github.com/mozilla/gmp-api/archive/Firefox39.tar.gz -> gmp-api-firefox-39.tar.gz"

LICENCES="BSD-2"
SLOT="0"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    nsplugin
    ( providers: libc++ libstdc++ ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/nasm
"

openh264_emake() {
    CC=${CC} CXX=${CXX} LD=${LD} \
    emake V=Yes PREFIX="/usr/$(exhost --target)"   \
    SHAREDLIB_DIR="/usr/$(exhost --target)/lib"    \
    INCLUDES_DIR="/usr/$(exhost --target)/include" \
    "${@}"
}

src_prepare() {
    edo ln -s "${WORKBASE}"/gmp-api-Firefox39 gmp-api
}

src_compile() {
    if [[ $(exhost --target) == i686-* ]]; then
        openh264_emake ENABLE64BIT=No
    else
        openh264_emake ENABLE64BIT=Yes
    fi

    optionq nsplugin && openh264_emake plugin
}

src_install() {
    local STATIC_LDFLAGS="-lpthread -lm"
    if option providers:libc++ ; then
        STATIC_LDFLAGS="-lc++ ${STATIC_LDFLAGS}"
    else
        STATIC_LDFLAGS="-lstdc++ ${STATIC_LDFLAGS}"
    fi

    openh264_emake install-shared \
        DESTDIR="${IMAGE}" \
        STATIC_LDFLAGS="${STATIC_LDFLAGS}"

    insinto /usr/$(exhost --target)/bin/
    dobin h264{enc,dec}

    if optionq nsplugin; then
        insinto /usr/$(exhost --target)/lib/nsbrowser/plugins/gmp-gmpopenh264/system-installed
        doins libgmpopenh264.so* gmpopenh264.info
        edo echo MOZ_GMP_PATH="/usr/$(exhost --target)/lib/nsbrowser/plugins/gmp-gmpopenh264/system-installed" \
            > "${TEMP}"/98-moz-openh264-gmp
        doenvd "${TEMP}"/98-moz-openh264-gmp

        # Avoid firefox overriding our installation of openh264
        cat <<PREFEOF >"${TEMP}"/openh264.js
pref("media.gmp-gmp${PN}.autoupdate", false);
pref("media.gmp-gmp${PN}.version", "system-installed");
PREFEOF

        insinto /usr/$(exhost --target)/lib/firefox/defaults/pref
        doins "${TEMP}"/openh264.js
    fi
}

