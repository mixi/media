# Copyright 2009 Daniel Mierswa <impulze@impulze.org>
# Copyright 2012 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN,,}
MY_PNV=${MY_PN}-${PV}

require github [ user=uclouvain tag=v${PV} ] cmake

SUMMARY="Open-Source JPEG2000 Codec"
DESCRIPTION="An Open-Source JPEG2000 Codec written in C."
HOMEPAGE+=" http://www.${PN}.org"

UPSTREAM_DOCUMENTATION="https://www.${PN}.org/doxygen [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="https://github.com/uclouvain/openjpeg/blob/v${PV}/NEWS.md [[ lang = en ]]"

LICENCES="BSD-2"
SLOT="2"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="tiff"

DEPENDENCIES="
    build+run:
        media-libs/lcms2
        media-libs/libpng:=
        sys-libs/zlib
        tiff? ( media-libs/tiff:= )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0535bfc3b7d5cd6fc73a7d4a6749a338fc5d7703.patch
    "${FILES}"/${PN}-2.4.0-CVE-2021-3575.patch
)

CMAKE_SOURCE="${WORKBASE}"/${MY_PNV}

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=( TIFF )

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_STATIC_LIBS:BOOL=FALSE
    -DBUILD_TESTING:BOOL=FALSE
    -DBUILD_THIRDPARTY:BOOL=FALSE
    -DBUILD_UNIT_TESTS:BOOL=FALSE
)

