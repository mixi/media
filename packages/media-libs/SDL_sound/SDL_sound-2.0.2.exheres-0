# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require github [ user=icculus pnv=${PNV/SDL/SDL2} release=v${PV} suffix=tar.gz ] cmake
require alternatives

SUMMARY="A library that handles the decoding of several popular sound file formats, such as .WAV and .MP3"
HOMEPAGE="https://icculus.org/${PN}/"

LICENCES="
    Artistic [[ note = [ src/timidity MIDI support ] ]]
    ZLIB
"
SLOT="$(ever range 1)"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/SDL:2
        !media-libs/SDL_sound:0[<1.0.3-r3] [[
            description = [ Require media-libs/SDL_sound:0 version with alternatives support ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_DISABLE_FIND_PACKAGE_Doxygen:BOOL=TRUE
    -DSDLSOUND_BUILD_SHARED:BOOL=TRUE
    -DSDLSOUND_BUILD_STATIC:BOOL=FALSE
    -DSDLSOUND_DECODER_AIFF:BOOL=TRUE
    -DSDLSOUND_DECODER_AU:BOOL=TRUE
    -DSDLSOUND_DECODER_FLAC:BOOL=TRUE
    -DSDLSOUND_DECODER_MIDI:BOOL=TRUE
    -DSDLSOUND_DECODER_MODPLUG:BOOL=TRUE
    -DSDLSOUND_DECODER_MP3:BOOL=TRUE
    -DSDLSOUND_DECODER_RAW:BOOL=TRUE
    -DSDLSOUND_DECODER_SHN:BOOL=TRUE
    -DSDLSOUND_DECODER_VOC:BOOL=TRUE
    -DSDLSOUND_DECODER_VORBIS:BOOL=TRUE
    -DSDLSOUND_DECODER_WAV:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DSDLSOUND_BUILD_TEST:BOOL=TRUE -DSDLSOUND_BUILD_TEST:BOOL=FALSE'
)

src_install() {
    cmake_src_install

    local arch_dependent_alternatives=()
    local host=$(exhost --target)

    arch_dependent_alternatives=(
        /usr/${host}/bin/playsound playsound-${SLOT}
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

