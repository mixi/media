# Copyright 2008-2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gstreamer

PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS+="
    backtrace [[ description = [ Generate backtraces using elfutils and libunwind ] ]]
    debug
    gobject-introspection
    ( libc: musl )
"

# Test run against / instead of $WORK, which makes them somewhat pointless
RESTRICT="test"

DEPENDENCIES+="
    build:
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.31.1] )
    build+run:
        dev-libs/glib:2[>=2.62.0]
        sys-libs/libcap
        backtrace? (
            dev-libs/libunwind
            dev-util/elfutils
            libc:musl? ( dev-libs/libexecinfo )
        )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dgst_parse=true
    -Dregistry=true
    -Dtracer_hooks=true

    -Dextra-checks=enabled
    -Doption-parsing=true
    -Dpoisoning=true
    -Dmemory-alignment=malloc

    -Dcheck=enabled
    -Dcoretracers=enabled
    -Dtests=enabled
    -Dtools=enabled

    -Dexamples=disabled
    -Dbenchmarks=disabled

    -Dgobject-cast-checks=disabled
    -Dglib-asserts=disabled
    -Dglib-checks=disabled

    -Ddoc=disabled
    -Dnls=enabled
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'debug gst_debug'
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'backtrace libdw'
    'backtrace libunwind'
    'bash-completion'
    'gobject-introspection introspection'
)

# Needed for async tests
DEFAULT_SRC_TEST_PARAMS=( -j1 )

