# Copyright 2008 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

OPTION_RENAMES=( 'av1 av1-decode' 'v4l2 v4l' 'vp8 vpx' )

ALTERNATIVES_ffmpeg_DESCRIPTION="System-wide FFmpeg version"

require toolchain-funcs option-renames alternatives

export_exlib_phases pkg_setup src_configure src_install

SUMMARY="A complete solution to record, convert and stream audio and video"
DESCRIPTION="
FFmpeg is a complete solution to record, convert and stream audio and video. It includes libavcodec,
the leading audio/video codec library. The project is made of several components:

    * ffmpeg is a command line tool to convert one video file format to another
      as well as grabbing and encoding in real time from a TV card.
    * ffplay is a simple media player based on SDL and on the FFmpeg libraries.
    * libavcodec is a library containing all the FFmpeg audio/video
      encoders and decoders.
    * libavformat is a library containing parsers and generators for all common
      audio/video formats.
"
HOMEPAGE="https://${PN}.org"
DOWNLOADS="${HOMEPAGE}/releases/${PNV}.tar.bz2"

UPSTREAM_CHANGELOG="https://git.videolan.org/?p=${PN}.git;a=shortlog;h=n${PV} [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/documentation.html [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="https://git.videolan.org/?p=${PN}.git;a=blob;f=RELEASE_NOTES;hb=release/$(ever range 1-2) [[ lang = en ]]"

GPL_OPTIONS=(
    cd
    frei0r
    hevc
    providers:openh264
    providers:x264
    rubberband
    samba
    teletext
    vidstab
    xavs
)

V3_OPTIONS=(
    lensfun
    opencore
    providers:mbedtls
    samba
)

# TODO: somehow express that according to configure they are only nonfree if combined with gpl
# options
# TODO: when using dev-libs/openssl:=[>=3.0.0] it can be moved to V3_OPTIONS
NONFREE_OPTIONS=(
    cuda
    fdk-aac
    nvenc
    providers:libressl
    providers:openssl
)

LICENCES="LGPL-2.1"

for opt in "${GPL_OPTIONS[@]}"; do
    LICENCES+=" ${opt}? ( GPL-2 )"
done
for opt in "${V3_OPTIONS[@]}"; do
    LICENCES+=" ${opt}? ( LGPL-3 )"
done

myexparam slot="$(ever major)"
exparam -v SLOT slot

# TODO: the nasm/yasm dep requires platform:, but that's not reliable when cross-compiling
MYOPTIONS="
    alsa         [[ description = [ Support sound recoding and playback using ALSA ] ]]
    ass          [[ description = [ Advanced SubStation Alpha (ASS) subtitles support ] ]]
    av1-encode   [[ description = [ Enable AV1 encoding ] ]]
    av1-encode? ( ( providers:
        rav1e [[ description = [ Enable AV1 encoding using rav1e ] ]]
        svt-av1 [[ description = [ Enable AV1 encoding using SVT-AV1 ] ]]
    ) [[ number-selected = at-least-one ]] )
    av1-decode   [[ description = [ AV1 decoding via libdav1d ] ]]
    bidi         [[ description = [ Enable support for text shaping in drawtext via libfribidi ] ]]
    bluray       [[ description = [ Support for reading Blu-ray discs ] ]]
    bs2b         [[ description = [ Enable support for libbs2b-based stereo-to-binaural audio filter ] ]]
    caca         [[ description = [ Support for colored ASCII video output ] ]]
    cd           [[ description = [ Audio CD grabbing ] ]]
    celt         [[ description = [ Support for the CELT low-delay audio codec ] ]]
    cpudetect    [[ description = [ Build all CPU features, but only use supported CPU features based on a detection at runtime ] ]]
    cuda [[
        description = [ NVIDIA hardware accelerated CUDA based image scaling ]
        requires = [ nvenc ]
    ]]
    doc
    fdk-aac      [[ description = [ Support for AAC encoding using the Frauenhofer AAC Codec Library, makes the resulting binary non-redistributable ] ]]
    fontconfig   [[ description = [ Support for managing custom fonts via fontconfig ] ]]
    frei0r       [[ description = [ Video effects using frei0r-plugins ] ]]
    gsm          [[ description = [ Support for GSM codec (audio), mainly for telephony ] ]]
    h264         [[ description = [ Enable H.264 encoding ] ]]
    h264? ( ( providers:
        openh264 [[ description = [ Enable H.264 encoding using openh264 ] ]]
        x264 [[ description = [ Enable H.264 encoding using x264 ] ]]
    ) [[ number-selected = at-least-one ]] )
    hap          [[ description = [ Hap video encoder and decoder ] ]]
    hevc         [[ description = [ Enable H.265/HEVC encoding using x265 ] ]]
    ieee1394     [[ description = [ Enable IIDC-1394 grabbing using libdc1394 ] ]]
    ilbc         [[ description = [ Support for decoding/encoding the Internet Low Bitrate Codec (iLBC) audio codec ] ]]
    jack
    jpeg2000     [[ description = [ Support for decoding/encoding lossy image compression format ] ]]
    kms          [[ description = [ KMS screen grabber input device ] ]]
    ladspa       [[ description = [ Linux Audio Developer Simple Plugin audio filtering support ] ]]
    lensfun      [[ description = [ Use the lensfun library for correcting optical lens defects ] ]]
    lv2          [[ description = [ Support for LV2 (simple but extensible successor of LADSPA) plugins ] ]]
    modplug      [[ description = [ ModPlug support, for MOD-like music files ] ]]
    mp2          [[ description = [ Enable MPEG Audio Layer 2 encoding through twolame ] ]]
    mp3          [[ description = [ Support for mp3 encoding with lame ] ]]
    nvenc        [[ description = [ Enable NVIDIA hardware accelerated Decoder/Encoder (NVDEC/NVENC) API ] ]]
    openal       [[ description = [ OpenAL capture support ] ]]
    opencl       [[ description = [ Hardware acceleration via OpenCL ] ]]
    opencore     [[ description = [ Support for OpenCORE AMR-WB decoder and encoder (audio) ] ]]
    opengl       [[ description = [ OpenGL rendering output ] ]]
    opus         [[ description = [ Enable support for Opus de/decoding via libopus ] ]]
    player       [[ requires = [ sdl ] description = [ FFmpeg reference audio and video player software ] ]]
    pulseaudio   [[ description = [ Pulseaudio capture support ] ]]
    rtmp         [[ description = [ RTMP (flash stream) support ] ]]
    rubberband   [[ description = [ Filter for time-stretching and pitch-shifting using librubberband ] ]]
    samba        [[ description = [ Enable support for the Samba protocol via libsmbclient ] ]]
    sdl          [[ description = [ Support output through SDL ] ]]
    sftp         [[ description = [ SFTP protocol support via libssh ] ]]
    sndio        [[ description = [ Adds support for recording and playback through sndio (OpenBSD sound API, also ported to Linux) ] ]]
    speex        [[ description = [ Enable support for decoding and encoding audio using libspeex ] ]]
    svg
    teletext     [[ description = [ Teletext support via libzvbi ] ]]
    theora       [[ description = [ Enable support for encoding using the Theora Video Compression Codec ] ]]
    truetype
    v4l          [[ description = [ Access V4L2 devices ] ]]
    va           [[ description = [ Enable support for decoding video using the Video Acceleration API ] ]]
    vdpau        [[ requires = [ X ] description = [ Enable support for VDPAU hardware accelerated video decoding ] ]]
    vidstab      [[ description = [ Analyze and perform video stabilization/deshaking ] ]]
    vorbis       [[ description = [ Additional OggVorbis audio de-/encoder plugin (ffmpeg's encoder is experimental) ] ]]
    vpx          [[ description = [ Enable support for VP7/VP8 and VP9 de/encoding via libvpx ] ]]
    vulkan       [[ description = [ Vulkan hardware acceleration support ] ]]
    webp         [[ description = [ WebP encoding via libwebp ] ]]
    X            [[ description = [ Enable support for X11 grabbing ] ]]
    xavs         [[ description = [ Support AVS, the Audio Video Standard of China ] ]]
    xml          [[ description = [ XML parsing using the C library libxml2 ] ]]
    xv           [[ description = [ Enable XVideo output, so ffmpeg can e.g. display the video while encoding ] ]]

    ( providers:
        gnutls
        libressl [[ description = [ Use LibreSSL instead of GnuTLS, makes the resulting binary non-redistributable ] ]]
        mbedtls
        openssl  [[ description = [ Use OpenSSL instead of GnuTLS, makes the resulting binary non-redistributable ] ]]
    ) [[ number-selected = exactly-one ]]

    ( platform: amd64 x86 )
"

for opt in "${NONFREE_OPTIONS[@]}"; do
    # We repeat the bindist [[ ]] part so the suboption prefix is only valid for the one option
    # we're handling at the moment. Otherwise we'd need to require a specific order in
    # NONFREE_OPTIONS. ()-grouping doesn't work here.
    if [[ "${opt}" == *:* ]]; then
        MYOPTIONS+=" bindist [[ requires = [ ${opt%:*}: -${opt#*:} ] ]]"
    else
        MYOPTIONS+=" bindist [[ requires = [ -${opt} ] ]]"
    fi
done

DEPENDENCIES="
    build:
        sys-apps/texinfo
        virtual/pkg-config
        nvenc? ( media-libs/nv-codec-headers[>=8.1.24.11] )
        opencl? ( dev-libs/opencl-headers )
        platform:amd64? ( dev-lang/nasm )
        platform:x86? ( dev-lang/nasm )
        vulkan? ( sys-libs/vulkan-headers[>=1.1.97] )
    build+run:
        app-arch/bzip2
        app-arch/xz
        sys-libs/zlib
        alsa? ( sys-sound/alsa-lib )
        ass? ( media-libs/libass )
        av1-encode? (
            providers:rav1e? ( media-video/rav1e[>=0.4.0] )
            providers:svt-av1? ( media-libs/SVT-AV1[>=0.8.4] )
        )
        av1-decode? ( media-libs/dav1d:=[>=0.5.0] )
        bidi? ( dev-libs/fribidi )
        bluray? ( media-libs/libbluray )
        bs2b? ( media-libs/libbs2b )
        caca? ( media-libs/libcaca )
        cd? ( dev-libs/libcdio-paranoia )
        celt? ( media-libs/celt:0.11 )
        cuda? ( dev-util/nvidia-cuda-toolkit )
        fdk-aac? ( media-libs/fdk-aac )
        fontconfig? ( media-libs/fontconfig )
        frei0r? ( media-plugins/frei0r-plugins )
        gsm? ( media-libs/gsm )
        h264? (
            providers:openh264? ( media-libs/openh264[>=1.3] )
            providers:x264? ( media-libs/x264:=[>=20120104] )
        )
        hap? ( app-arch/snappy )
        hevc? ( media-libs/x265:=[>=1.9] )
        ieee1394? (
            media-libs/libdc1394:2
            media-libs/libraw1394
        )
        ilbc? ( media-libs/ilbc )
        jack? ( media-sound/jack-audio-connection-kit )
        jpeg2000? ( media-libs/OpenJPEG:2[>=2.1] )
        kms? ( x11-dri/libdrm )
        ladspa? ( media-libs/ladspa-sdk )
        lensfun? ( media-libs/lensfun )
        lv2? (
            media-libs/lilv
            media-libs/lv2
        )
        modplug? ( media-libs/libmodplug )
        mp2? ( media-libs/twolame[>=0.3.10] )
        mp3? ( media-sound/lame[>=3.98.3] )
        openal? ( media-libs/openal )
        opencl? ( dev-libs/opencl-icd-loader )
        opencore? (
            media-libs/opencore-amr
            media-libs/vo-amrwbenc
        )
        opengl? ( dev-libs/libglvnd )
        opus? ( media-libs/opus )
        providers:gnutls? (
            dev-libs/gmp:=
            dev-libs/gnutls
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:mbedtls? ( dev-libs/mbedtls )
        providers:openssl? ( dev-libs/openssl:= )
        pulseaudio? ( media-sound/pulseaudio )
        rtmp? ( media-video/rtmpdump[>=2.2] )
        rubberband? ( media-libs/rubberband[>=1.8.1] )
        samba? ( net-fs/samba )
        sdl? ( media-libs/SDL:2[>=2.0.1] )
        sftp? ( net-libs/libssh )
        sndio? ( sys-sound/sndio )
        speex? ( media-libs/speex )
        svg? (
            gnome-desktop/librsvg:2
            x11-libs/cairo
        )
        teletext? ( media-libs/zvbi )
        theora? (
            media-libs/libogg
            media-libs/libtheora
        )
        truetype? ( media-libs/freetype:2 )
        v4l? ( media-libs/v4l-utils )
        va? ( x11-libs/libva[>=1.0.6] )
        vdpau? ( x11-libs/libvdpau[>=0.2] )
        vidstab? ( media-libs/libvidstab[>=0.98] )
        vorbis? (
            media-libs/libogg
            media-libs/libvorbis
        )
        vpx? ( media-libs/libvpx:=[>=1.3.0] )
        vulkan? ( sys-libs/vulkan-loader[>=1.1.97] )
        webp? ( media-libs/libwebp:=[>=0.4.0] )
        X? (
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXv
            x11-libs/libxcb[>=1.4]
        )
        xavs? ( media-libs/xavs )
        xml? ( dev-libs/libxml2:2.0 )
        xv? ( x11-libs/libXv )
        !media/libav [[ description = [ Libav is a fork of ffmpeg and uses the same file names ] ]]

"
if [[ $SLOT != 0 ]] ; then
    DEPENDENCIES+="
        build+run:
            !media/ffmpeg:0[>=4&<4.4.4-r2] [[
                description = [ Require media/ffmpeg:0 version with alternatives support ]
                resolution = [ upgrade-blocked-before ]
            ]]
            !media/ffmpeg:0[>=5&<5.1.3-r2] [[
                description = [ Require media/ffmpeg:0 version with alternatives support ]
                resolution = [ upgrade-blocked-before ]
            ]]
    "
fi
if [[ $SLOT == 6 ]] ; then
    DEPENDENCIES+="
        build+run:
            !media/ffmpeg:0[=6*] [[
                description = [
                    Masked media/ffmpeg-6.0 has moved slots. Please reinstall media/ffmpeg:0 before
                    installing media/ffmpeg:6 and run cave fix-linkage afterwards.
                ]
                resolution = [ manual ]
            ]]
    "
fi

if ever at_least 5.1; then
    # shaderc and glslang are mutually exclusive and shaderc is prefered by upstream so there is no
    # benefit of offering both.
    MYOPTIONS+="
        jpegxl
        lcms
        placebo [[
            description = [ Filters based on libplacebo ]
            requires = [ vulkan ]
        ]]
        shaderc [[ description = [ Filters based on Vulkan ] ]]
    "
    DEPENDENCIES+="
        build:
            dev-libs/libxml2:2.0 [[ note = [ xmllint ] ]]
            vulkan? ( sys-libs/vulkan-headers[>=1.2.189] )
        build+run:
            ass? ( media-libs/libass[>=0.11.0] )
            jpegxl? ( media-libs/libjxl:=[>=0.7rc] )
            lcms? ( media-libs/lcms2[>=2.13] )
            placebo? ( media-libs/libplacebo:=[>=4.192.0] )
            shaderc? ( sys-libs/shaderc[>=2019.1] )
            vulkan? ( sys-libs/vulkan-loader[>=1.2.189] )
    "
else
    MYOPTIONS+="
        glslang [[ description = [ Filters based on Vulkan ] ]]
    "
    DEPENDENCIES+="
        build+run:
            glslang? (
                dev-lang/glslang[>=11.0.0]
                dev-lang/spirv-tools
            )
    "
fi

if ever at_least 6.1; then
    DEPENDENCIES+="
        build:
            nvenc? ( media-libs/nv-codec-headers[>=8.1.24.15] )
            vulkan? ( sys-libs/vulkan-headers[>=1.3.255] )
        build+run:
            av1-encode? (
                providers:rav1e? ( media-video/rav1e[>=0.5.0] )
            )
            h264? (
                providers:x264? ( media-libs/x264:=[>=20120307] )
            )
            hevc? ( media-libs/x265:=[>=2.1] )
            openal? ( media-libs/openal[>=1.1] )
            sftp? ( net-libs/libssh[>=0.6.0] )
            vulkan? ( sys-libs/vulkan-loader[>=1.3.255] )
    "
fi

DEFAULT_SRC_INSTALL_PARAMS=( install-man )

FFMPEG_LIBDIRS=(
    libavcodec
    libavdevice
    libavfilter
    libavformat
    libavutil
    libpostproc
    libswresample
    libswscale
)

if ! ever at_least 5.1; then
    FFMPEG_LIBDIRS+=(
        libavresample
    )
fi

DEFAULT_SRC_TEST_PARAMS=( LD_LIBRARY_PATH=$(IFS=:; echo "${FFMPEG_LIBDIRS[*]}" ) fate )

ffmpeg-cc_cpu_macro_test_enable() {
    if optionq cpudetect || cc-macro-is-true ${1}; then
        echo --enable-${2}
    else
        echo --disable-${2}
    fi
}

ffmpeg-cc_cpu_define_enable() {
    if optionq cpudetect || cc-has-defined ${1}; then
        echo --enable-${2}
    else
        echo --disable-${2}
    fi
}

ffmpeg-build_pkg_setup() {
    export V=1
}

# grayscale support is disabled
ffmpeg-build_src_configure() {
    local myconf=() target=$(exhost --target)

    # licences
    local opt gpl_param="" v3_param="" nonfree_param=""

    for opt in "${GPL_OPTIONS[@]}"; do
        option "${opt}" && gpl_param="--enable-gpl"
    done
    for opt in "${V3_OPTIONS[@]}"; do
        option "${opt}" && v3_param="--enable-version3" # LGPL-3 or GPL-3 depending on gpl_param
    done
    for opt in "${NONFREE_OPTIONS[@]}"; do
        option "${opt}" && nonfree_param="--enable-nonfree" # not redistributable
    done

    myconf+=(
        ${gpl_param}
        ${v3_param}
        ${nonfree_param}
    )

    #FIXME this configure script doesn't handle --host
    myconf+=(
        --ar="${AR}"
        --nm="${NM}"
        --cc="${CC}"
        --host-cflags="${CFLAGS}" # respect user cflags and avoid -O3 -g
        --cxx="${CXX}"
        --extra-cxxflags="${CXXFLAGS}" # we have currently no other way to use user flags
        --pkg-config="${PKG_CONFIG}"
        --ranlib="${RANLIB}"

        --prefix=/usr/$(exhost --target)
        --datadir=/usr/share/${PN}-${SLOT}
        --docdir=/usr/share/doc/${PNVR}
        --incdir=/usr/$(exhost --target)/include/${PN}-${SLOT}
        --mandir=/usr/share/man

        --progs-suffix=-${SLOT}
        --build-suffix=-${SLOT}

        ### CPU features
        --arch=${target%%-*}

        $(
            case "$(exhost --target)" in
                # x86
                i*86-*|x86_64-*)
                    ffmpeg-cc_cpu_define_enable __MMX__ x86asm
                    ffmpeg-cc_cpu_define_enable __MMX__ mmx
                    # most of ffmpeg's mmxext functions have an sse2 alternative with higher priority anyways
                    # ffmpeg-cc_cpu_define_enable TODO mmxext
                    ffmpeg-cc_cpu_define_enable __3dNOW__ amd3dnow
                    # ffmpeg uses pfpnacc which is part of 3dNOW_A according to gcc sources
                    ffmpeg-cc_cpu_define_enable __3dNOW_A__ amd3dnowext
                    ffmpeg-cc_cpu_define_enable __AESNI__ aesni
                    ffmpeg-cc_cpu_define_enable __AVX__ avx
                    ffmpeg-cc_cpu_define_enable __AVX2__ avx2
                    ffmpeg-cc_cpu_define_enable __AVX512__ avx512
                    ffmpeg-cc_cpu_define_enable __FMA__ fma3
                    ffmpeg-cc_cpu_define_enable __FMA4_ fma4
                    ffmpeg-cc_cpu_define_enable __SSE__ sse
                    ffmpeg-cc_cpu_define_enable __SSE2__ sse2
                    ffmpeg-cc_cpu_define_enable __SSE3__ sse3
                    ffmpeg-cc_cpu_define_enable __SSSE3__ ssse3
                    ffmpeg-cc_cpu_define_enable __SSE4_1__ sse4
                    ffmpeg-cc_cpu_define_enable __SSE4_2__ sse42
                    ffmpeg-cc_cpu_define_enable __XOP__ xop
                    ;;
                ## arm
                # aarch32
                arm*)
                    ffmpeg-cc_cpu_macro_test_enable "!defined(__SOFTFP__) && __ARM_ARCH >= 7" \
                                                    vfpv3
                    ffmpeg-cc_cpu_macro_test_enable "defined(__ARM_ARCH_5TE__) || defined(__ARM_ARCH_5TEJ__) || __ARM_ARCH > 5" \
                                                    armv5te
                    ffmpeg-cc_cpu_macro_test_enable "__ARM_ARCH >= 6" armv6
                    ffmpeg-cc_cpu_macro_test_enable "defined(__ARM_ARCH6T2__) || __ARM_ARCH > 6" armv6t2
                    # On aarch32 __VFP_FP__ is always advertised, even for
                    # softfp, so disable it only in that case.
                    ffmpeg-cc_cpu_macro_test_enable "!defined(__SOFTFP__)" vfp
                    ffmpeg-cc_cpu_macro_test_enable "defined(__ARM_NEON)" neon
                    ;;
                aarch64-*)
                    # On aarch64 both vfp and neon are always enabled. It is also always armv8.
                    echo --enable-vfp --enable-neon --enable-armv8
                    ;;
            esac
        )

        ### hard enable

        # system related
        --enable-bzlib
        --enable-iconv
        --enable-lzma
        --enable-shared
        --enable-pthreads
        --enable-zlib
        --disable-autodetect
        --disable-debug
        --disable-linux-perf
        --disable-optimizations # optimizations is just a stupid way of enabling -O3 -fomit-frame-pointer
        --disable-static
        --disable-stripping

        # prefer librtmp for rtmp(t)e support
        --disable-gcrypt
        --disable-gmp
        # prefer the native encoder of FFmpeg over using libxvid
        --disable-libxvid
        # broken with recent opencv versions, upstream appears to have little interest in this
        --disable-libopencv

        # wrong platform, but disable autodetect
        --disable-amf
        --disable-appkit
        --disable-audiotoolbox
        --disable-avfoundation
        --disable-coreimage
        --disable-d3d11va
        --disable-dxva2
        --disable-jni
        --disable-mediacodec
        --disable-mediafoundation
        --disable-schannel
        --disable-securetransport
        --disable-videotoolbox

        # missing dependency
        --disable-decklink
        --disable-libaom
        --disable-libaribb24
        --disable-libcodec2
        --disable-libdavs2
        --disable-libflite
        --disable-libgme
        --disable-libklvanc
        --disable-libkvazaar
        --disable-libmfx
        --disable-libmysofa
        --disable-libopenmpt
        --disable-libopenvino
        --disable-librist
        --disable-libshine
        --disable-libsoxr
        --disable-libsrt
        --disable-libtensorflow
        --disable-libtls
        --disable-libuavs3d
        --disable-libvmaf
        --disable-libxavs2
        --disable-libzimg
        --disable-mmal
        --disable-omx
        --disable-omx-rpi
        --disable-pocketsphinx
        --disable-rkmpp
        --disable-vapoursynth

        # not tested
        --disable-chromaprint
        --disable-librabbitmq
        --disable-libtesseract
        --disable-libzmq

        ### OPTIONS
        $(option_enable alsa)
        $(option_enable ass libass)
        $(option_enable av1-decode libdav1d)
        $(option_enable bidi libfribidi)
        $(option_enable bluray libbluray)
        $(option_enable bs2b libbs2b)
        $(option_enable caca libcaca)
        $(option_enable cd libcdio)
        $(option_enable celt libcelt)
        $(option_enable cpudetect runtime-cpudetect)
        $(option_enable doc)
        $(option_enable doc htmlpages)
        $(option_enable fdk-aac libfdk-aac)
        $(option_enable fontconfig)
        $(option_enable fontconfig libfontconfig)
        $(option_enable frei0r)
        $(option_enable gsm libgsm)
        $(option_enable hap libsnappy)
        $(option_enable hevc libx265)
        $(option_enable ieee1394 libdc1394)
        $(option_enable ilbc libilbc)
        $(option_enable jack libjack)
        $(option_enable jpeg2000 libopenjpeg)
        $(option_enable kms libdrm)
        $(option_enable ladspa)
        $(option_enable lensfun liblensfun)
        $(option_enable lv2)
        $(option_enable modplug libmodplug)
        $(option_enable mp2 libtwolame)
        $(option_enable mp3 libmp3lame)
        $(option_enable nvenc)
        $(option_enable nvenc cuvid)
        $(option_enable nvenc ffnvcodec)
        $(option_enable nvenc nvdec)
        $(option_enable openal)
        $(option_enable opencl)
        $(option_enable opencore libopencore-amrnb)
        $(option_enable opencore libopencore-amrwb)
        $(option_enable opencore libvo-amrwbenc)
        $(option_enable opengl)
        $(option_enable opus libopus)
        $(option_enable player ffplay)
        $(option_enable providers:mbedtls mbedtls)
        $(option_enable pulseaudio libpulse)
        $(option_enable rtmp librtmp)
        $(option_enable rubberband librubberband)
        $(option_enable samba libsmbclient)
        $(option_enable sdl sdl2)
        $(option_enable sftp libssh)
        $(option_enable sndio)
        $(option_enable speex libspeex)
        $(option_enable svg librsvg)
        $(option_enable teletext libzvbi)
        $(option_enable theora libtheora)
        $(option_enable truetype libfreetype)
        $(option_enable v4l libv4l2)
        $(option_enable v4l v4l2-m2m)
        $(option_enable va vaapi)
        $(option_enable vdpau)
        $(option_enable vidstab libvidstab)
        $(option_enable vorbis libvorbis)
        $(option_enable vpx libvpx)
        $(option_enable vulkan)
        $(option_enable webp libwebp)
        $(option_enable X libxcb)
        $(option_enable X libxcb-shm)
        $(option_enable X libxcb-xfixes)
        $(option_enable X libxcb-shape)
        $(option_enable X xlib)
        $(option_enable xavs libxavs)
        $(option_enable xml libxml2)
        $(option alsa || echo "--disable-indev=alsa")
        $(option alsa || echo "--disable-outdev=alsa")
        $(option xv || echo "--disable-outdev=xv")
    )

    if option h264 ; then
        myconf+=(
            $(option_enable providers:openh264 libopenh264)
            $(option_enable providers:x264 libx264)
        )
    else
        myconf+=(
            --disable-libopenh264
            --disable-libx264
        )
    fi

    if option av1-encode; then
        myconf+=(
            $(option_enable providers:rav1e librav1e)
            $(option_enable providers:svt-av1 libsvtav1)
        )
    else
        myconf+=(
            --disable-librav1e
            --disable-libsvtav1
        )
    fi

    # NVIDIA CUDA
    myconf+=(
        --disable-cuda-llvm
        $(option_enable cuda cuda-nvcc)
        $(option_enable cuda libnpp)

    )
    if option cuda ; then
        myconf+=(
            --extra-cflags="${CFLAGS} -I/opt/cuda/include"
            --extra-ldflags="${LDFLAGS} -L/opt/cuda/lib64"
            --nvcc=/opt/cuda/bin/nvcc
            --nvccflags="--compiler-bindir=/opt/cuda/bin/gcc"
        )
    fi

    myconf+=(
        $(option_enable providers:gnutls gnutls)
    )
    if option providers:libressl || option providers:openssl ; then
        myconf+=( --enable-openssl )
    else
        myconf+=( --disable-openssl )
    fi

    if ever at_least 5.1; then
        myconf+=(
            $(
                case "$(exhost --target)" in
                    # x86
                    i*86-*|x86_64-*)
                        ffmpeg-cc_cpu_define_enable __AVX512ICL__ avx512icl
                        ;;
                esac
            )

            --enable-ptx-compression
            --disable-libglslang
            --disable-macos-kperf
            --disable-metal
            $(option_enable jpegxl libjxl)
            $(option_enable lcms lcms2)
            $(option_enable placebo libplacebo)
            $(option_enable shaderc libshaderc)
        )
    else
        myconf+=(
            # system related
            --enable-lzo
            # libav compatibility (deprecated)
            --enable-avresample
            $(option_enable glslang libglslang)
        )
    fi

    if ever at_least 6.1; then
        myconf+=(
           --disable-libaribcaption
           --disable-libharfbuzz
           --disable-libvpl
        )
    fi

    edo ./configure "${myconf[@]}"
}

ffmpeg-build_src_install() {
    default

    local target=$(exhost --target)

    # Examples really should be considered docs, move them to the docs directory.
    edo mv "${IMAGE}"/usr/share/ffmpeg-${SLOT}/examples "${IMAGE}"/usr/share/doc/${PNVR}/examples

    local -a ffmpeg_bins=( ffmpeg ffprobe )
    optionq player && ffmpeg_bins+=( ffplay )
    local -a man1s=(
        ffmpeg-bitstream-filters
        ffmpeg-codecs
        ffmpeg-devices
        ffmpeg-filters
        ffmpeg-formats
        ffmpeg-protocols
        ffmpeg-resampler
        ffmpeg-scaler
        ffmpeg-utils
    )

    local -a ffmpeg_alternatives=(
        # Technically not required, but this is where the user expects to find the files
        /usr/share/${PN} ${PN}-${SLOT}
    )
    local bin lib man1 man3
    for bin in "${ffmpeg_bins[@]}" ; do
        ffmpeg_alternatives+=( /usr/${target}/bin/${bin} ${bin}-${SLOT} )
        man1s+=( ${bin} ${bin}-all )
    done
    for lib in "${FFMPEG_LIBDIRS[@]}" ; do
        ffmpeg_alternatives+=(
            # include & lib symlinks aren't needed when using pkg-config, but not all apps use it.
            /usr/${target}/include/${lib} ${PN}-${SLOT}/${lib}
            /usr/${target}/lib/${lib}.so ${lib}-${SLOT}.so
            /usr/${target}/lib/pkgconfig/${lib}.pc ${lib}-${SLOT}.pc
        )
        if [[ -f "${IMAGE}"/usr/share/man/man3/${lib}.3 ]] ; then
            ffmpeg_alternatives+=( /usr/share/man/man3/${lib}.3 ${lib}-${SLOT}.3 )
        fi

        # For applications that attempt to dynamically open ffmpeg at runtime, add symlinks to
        # the original sonames. These have to be handled with alternatives-light, since multiple
        # ffmpeg slots can install the same library versions.
        fullname=$(readlink "${IMAGE}/usr/${target}/lib/${lib}-${SLOT}.so")
        local soname="${fullname%.*.*}"
        local ext="${soname#*.}"
        alternatives_for _${target}_${lib}.${ext} ${SLOT} ${SLOT} /usr/${target}/lib/${lib}.${ext} ${lib}-${SLOT}.${ext}
    done
    for man1 in "${man1s[@]}" ; do
        ffmpeg_alternatives+=( /usr/share/man/man1/${man1}.1 ${man1}-${SLOT}.1 )
    done

    alternatives_for ffmpeg ${SLOT} ${SLOT} "${ffmpeg_alternatives[@]}"
}

