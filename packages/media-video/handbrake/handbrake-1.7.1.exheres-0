# Copyright 2013-2014 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'handbrake-0.9.9_pre5441-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2013 Gentoo Foundation

MY_PNV="${PNV/handbrake/HandBrake}-source"

require github [ release=${PV} suffix=tar.bz2 ]
require autotools [ supported_autoconf=[ 2.7 ] supported_automake=[ 1.16 ] ]
require ffmpeg [ abis=[ 6 ] min_versions=[ 6.1 ] options="[av1-encode][providers:svt-av1]" ]

SUMMARY="Tool to convert video from nearly any format to modern codecs"
HOMEPAGE+=" https://${PN}.fr"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    hevc [[ description = [ Enable H.265/HEVC encoding using x265 ] ]]
    numa [[
        description = [ Enable NUMA support via numactl/libnuma ]
        requires = [ hevc ]
    ]]
    nvenc [[ description = [ Enable NVIDIA hardware accelerated Decoder/Encoder (NVDEC/NVENC) API ] ]]
"

DEPENDENCIES="
    build:
        dev-lang/nasm[>=2.13.0]
        dev-lang/python:*[>=3]
        sys-devel/cmake
        sys-devel/meson
        virtual/pkg-config[>=0.9.0]
        nvenc? ( media-libs/nv-codec-headers )
    build+run:
        app-arch/bzip2
        app-arch/xz
        dev-libs/fribidi
        dev-libs/jansson
        dev-libs/libxml2:2.0
        media-libs/dav1d:=
        media-libs/fdk-aac
        media-libs/fontconfig
        media-libs/freetype:2
        media-libs/libass
        media-libs/libbluray[>=1.0.0]
        media-libs/libdvdnav
        media-libs/libdvdread
        media-libs/libjpeg-turbo
        media-libs/libogg
        media-libs/libtheora
        media-libs/libvorbis
        media-libs/libvpx:=
        media-libs/opus
        media-libs/speex
        media-libs/SVT-AV1
        media-libs/x264:=
        media-libs/zimg
        media-sound/lame
        sys-libs/zlib
        x11-libs/harfbuzz
        hevc? ( media-libs/x265:= )
        numa? ( sys-apps/numactl )
    recommendation:
        media-video/handbrake-gui [[ description = [ Provides a GTK-based GUI ] ]]
"

WORK=${WORKBASE}/${MY_PNV//-source}

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0003-Remove-libdvdnav-duplication-and-call-it-on-the-orig.patch
    "${FILES}"/${PN}-1.7.0-revert-av_pkt_data_ambient_viewing_environment.patch
    "${FILES}"/${PN}-1.7.0-revert-x265_ambient_viewing_environment.patch
)

src_prepare() {
    default

    # Get rid of leftover bundled library build definitions,
    # the version 0.9.9 supports the use of system libraries.
    edo sed \
        -e 's:.*\(/contrib\|contrib/\).*::g' \
        -i make/include/main.defs

    # Configure runs version probe on the (banned) "pkg-config" even if the correct executable is
    # passed as an option.
    edo sed \
        -e "s:'pkg-config':'$(exhost --tool-prefix)pkg-config':g" \
        -i make/configure.py

    # undefined reference to symbol 'x265_api_query'
    if option hevc ; then
        edo sed \
            -e 's:x264:x264 x265:g' \
            -i test/module.defs
    fi

    # The only way I can find to get the gtk ui to use the correct ffmpeg libs is to put them into
    # the hardcoded 'contrib' directories.
    edo mkdir -p build/contrib
    edo ln -s "$(ffmpeg_alternatives_prefix)"/{include,lib} build/contrib
}

src_configure() {
    CC=cc edo python3 make/configure.py \
        --cross="$(exhost --target)" \
        --pkgconfig="${PKG_CONFIG}" \
        --force \
        --prefix="/usr/$(exhost --target)" \
        --enable-fdk-aac \
        --disable-df-fetch \
        --disable-df-verify \
        --disable-gst \
        --disable-gtk \
        --disable-gtk4 \
        --disable-libdovi \
        --disable-mf \
        --disable-qsv \
        --disable-vce \
        $(option_enable hevc x265) \
        $(option_enable numa) \
        $(option_enable nvenc) \
        $(option_enable nvenc nvdec)

    edo cat >build/GNUmakefile.custom.defs <<DEFS
GCC.args.g.none =
GCC.args.strip =
GCC.I += $(ffmpeg_alternatives_prefix)/include
TEST.GCC.L += $(ffmpeg_alternatives_prefix)/lib
DEFS
}

src_compile() {
    # The PKGCONFIG.exe variable does get set correctly in the generated GNUmakefile, but the point
    # where it gets used is before where it gets set, so the wrong pkg-config is run.
    emake -C build PKGCONFIG.exe="${PKG_CONFIG}"
}

src_install() {
    emake -C build DESTDIR="${IMAGE}" install

    # required for media-video/handbrake-gui
    dolib build/libhb/libhandbrake.a
    insinto /usr/$(exhost --target)/include/handbrake
    doins build/libhb/handbrake/project.h

    emagicdocs

    if [[ -d "${IMAGE}"/usr/$(exhost --target)/share ]]; then
        edo mv "${IMAGE}"/usr/$(exhost --target)/share/* "${IMAGE}"/usr/share/
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/share
    fi
}

